**ValidationPerson**

This is a back-end project developed in Java 17, also utilizing Docker for the database. To start the project, run the command "docker-compose up" to initialize the database.

The project implements CRUD operations. Each function has its corresponding endpoint. To register a new person in the database (POST), use the following endpoint: localhost:8080/register/create. 

The request body should be a JSON in the following format:

```
{
"name": "Giovanna",
"cpf": "80890017000",
"rg": "987654321",
"date_of_birth": "1990-01-01",
"date_register": "2023-07-06",
"address": {
    "publicPlace": "New Street XYZ",
    "homeNumber": 456,
    "neighborhood": "New Neighborhood ABC",
    "complement": "Apartment 789",
    "cep": "98765-432"
    },
    "contacts": {
    "email": "john.doe@example.com",
    "phone": "+5511962722649"
    }
}
```
To retrieve a list of all registered users from the database (GET), use the endpoint: localhost:8080/register/registerAll.

To retrieve the data of a specific person by CPF (GET), use the endpoint: localhost:8080/register/cpf=80890017000.
```
{
    "name": "Giovanna",
    "cpf": "80890017000",
    "rg": "987654321",
    "address": {
        "publicPlace": "New Street XYZ",
        "homeNumber": 456,
        "neighborhood": "New Neighborhood ABC",
        "complement": "Apartment 789",
        "cep": "98765-432"
    },
    "date_of_birth": "1990-01-01",
    "date_register": "2023-07-06",
    "contacts": {
        "email": "john.doe@example.com",
        "phone": "+5511962722649"
    }
}
.
.
.
```
To update the registration data of a specific person by CPF (PUT), use the endpoint: localhost:8080/register/cpf=0000000000. 

The request body should be a JSON in the following format:
```
{
  "address": {
    "publicPlace": "TESTANDO"
  },
  "contacts": {
    "email": "new.email@example.com"
    }
}
```


OBS.1: Este json é só um exemplo, porém com excessão do CPF, RG, e Data de Registro é possível realizar a atualização cadastral de todas as colunas na tabela;

Após ser atualizado temos o resultado:
```
{
    "name": "Giovanna",
    "cpf": "80890017000",
    "rg": "987654321",
    "address": {
        "publicPlace": "TESTANDO",
        "homeNumber": 456,
        "neighborhood": "New Neighborhood ABC",
        "complement": "Apartment 789",
        "cep": "98765-432"
    },
    "date_of_birth": "1990-01-01",
    "date_register": "2023-07-06",
    "contacts": {
        "email": "new.email@example.com",
        "phone": "+5511962722649"
    }
}
```
To delete the data of a specific person by CPF (DELETE), use the endpoint: localhost:8080/register/cpf=000000000.
Result: "Deletado com sucesso!"

In the project, there are three entities: Person, Address, and Contacts, responsible for creating the tables in the database.

Entities Overview:

Person:

Primary Key: id (auto-generated)
Represents a person entity with attributes such as name, cpf (Brazilian national identification number), rg (identity document), address, date_of_birth, date_register, and contacts.
It is associated with the Address entity through a one-to-one relationship and with the Contacts entity through another one-to-one relationship.
Address:

Primary Key: id (auto-generated)
Represents an address entity with attributes such as publicPlace, homeNumber, neighborhood, complement, and CEP (Brazilian postal code).
It is associated with the Person entity through a one-to-one relationship.
Contacts:

Primary Key: id (auto-generated)
Represents contact information for a person, including attributes such as email and phone.
It is associated with the Person entity through a one-to-one relationship.
In this data model, the Person entity serves as the central entity representing a person, while the Address and Contacts entities are linked to the Person entity through one-to-one relationships. Each entity has its own primary key (id) to uniquely identify records in the corresponding database table.

Note: The primary keys (id) are auto-generated, meaning that the database system assigns a unique value to them automatically upon record creation.

Remarks:

Make sure to run the project to initialize it.
This project can be paired with the FormValidation project, which is the corresponding front-end.
