package validate.person.api.Controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import validate.person.api.person.Address;
import validate.person.api.person.Contacts;
import validate.person.api.person.Person;
import validate.person.api.repository.PersonRepository;
import validate.person.api.service.PersonService;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@SpringBootTest
public class PersonServiceTest {

    @Mock
    private PersonRepository personRepository;

    @Mock
    private ObjectMapper objectMapper;

    @InjectMocks
    private PersonService personService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void createNewPersonTest() {
        Person person = new Person();
        person.setName("John Doe");
        person.setCpf("12345678900");
        person.setName("John Doe");
        person.setCpf("12345678900");
        person.setRg("987654321");
        person.setDate_of_birth("1990-01-01");
        person.setDate_register("2023-07-06");

        Address address = new Address();
        address.setPublicPlace("Street ABC");
        address.setHomeNumber(123);
        address.setNeighborhood("Neighborhood XYZ");
        address.setComplement("Apartment 456");
        address.setCEP("12345-678");
        person.setAddress(address);

        Contacts contacts = new Contacts();
        contacts.setEmail("john.doe@example.com");
        contacts.setPhone("123456789");
        person.setContacts(contacts);

        when(personRepository.save(person)).thenReturn(person);
        personService.createNewPerson(person);
        verify(personRepository).save(person);
    }

    @Test
    public void consultaDeCpfExistenteNoBancoTest() {
        String cpf = "123456789";
        boolean exist = false;

        Person person = new Person();
        person.setCpf(cpf);
        when(personRepository.findByCpf(cpf)).thenReturn(person);

        if(personRepository.findByCpf(cpf) != null){
            exist = true;
        }

        assertEquals(true, exist);
    }

    @Test
    public void deletarCpfTest(){
        String cpf = "12345678900";

        Person existingPerson = new Person();
        existingPerson.setId(1L);
        existingPerson.setName("John Doe");
        existingPerson.setCpf(cpf);
        existingPerson.setRg("987654321");
        existingPerson.setDate_of_birth("1990-01-01");
        existingPerson.setDate_register("2023-07-06");

        Address existingAddress = new Address();
        existingAddress.setPublicPlace("Street ABC");
        existingAddress.setHomeNumber(123);
        existingAddress.setNeighborhood("Neighborhood XYZ");
        existingAddress.setComplement("Apartment 456");
        existingAddress.setCEP("12345-678");
        existingPerson.setAddress(existingAddress);

        Contacts existingContacts = new Contacts();
        existingContacts.setEmail("john.doe@example.com");
        existingContacts.setPhone("123456789");
        existingPerson.setContacts(existingContacts);

        when(personRepository.findByCpf(cpf)).thenReturn(existingPerson);

        personService.deletePerson(cpf);

        verify(personRepository, times(1)).findByCpf(cpf);
        verify(personRepository, times(1)).deleteById(existingPerson.getId());

    }
}
