package validate.person.api.Controller;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import validate.person.api.controller.PersonController;
import validate.person.api.dto.AddressDTO;
import validate.person.api.dto.ContactsDTO;
import validate.person.api.dto.PersonDTO;
import validate.person.api.service.PersonService;


import static org.mockito.Mockito.*;

@SpringBootTest
public class PersonControllerTest {

    @Mock
    private PersonService personService;

    @InjectMocks
    private PersonController personController;


    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }


    @Test
    public void verificacaoCpfCorretoTest() {
        String cpf = "12345678900";

        PersonDTO personDto = new PersonDTO();
        personDto.setName("John Doe");
        personDto.setCpf("12345678900");
        personDto.setName("John Doe");
        personDto.setCpf("12345678900");
        personDto.setRg("987654321");
        personDto.setDate_of_birth("1990-01-01");
        personDto.setDate_register("2023-07-06");

        AddressDTO addressDTO = new AddressDTO();
        addressDTO.setPublicPlace("Street ABC");
        addressDTO.setHomeNumber(123);
        addressDTO.setNeighborhood("Neighborhood XYZ");
        addressDTO.setComplement("Apartment 456");
        addressDTO.setCEP("12345-678");
        personDto.setAddress(addressDTO);

        ContactsDTO contactsDTO = new ContactsDTO();
        contactsDTO.setEmail("john.doe@example.com");
        contactsDTO.setPhone("123456789");
        personDto.setContacts(contactsDTO);

        when(personService.consultPerson(cpf)).thenReturn(personDto);

        ResponseEntity<?> response = personController.register(cpf);

        Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
        verify(personService).consultPerson(cpf);
    }

    @Test
    public void verificacaoCpfIncorretoTest() {
        String cpfIncorrect = "12300000";

        PersonDTO personDto = new PersonDTO();
        personDto.setName("John Doe");
        personDto.setCpf("12345678900");
        personDto.setName("John Doe");
        personDto.setCpf("12345678900");
        personDto.setRg("987654321");
        personDto.setDate_of_birth("1990-01-01");
        personDto.setDate_register("2023-07-06");

        AddressDTO addressDTO = new AddressDTO();
        addressDTO.setPublicPlace("Street ABC");
        addressDTO.setHomeNumber(123);
        addressDTO.setNeighborhood("Neighborhood XYZ");
        addressDTO.setComplement("Apartment 456");
        addressDTO.setCEP("12345-678");
        personDto.setAddress(addressDTO);

        ContactsDTO contactsDTO = new ContactsDTO();
        contactsDTO.setEmail("john.doe@example.com");
        contactsDTO.setPhone("123456789");
        personDto.setContacts(contactsDTO);

        when(personService.consultPerson(cpfIncorrect)).thenReturn(null);

        ResponseEntity<?> response = personController.register(cpfIncorrect);
        Assertions.assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        verify(personService).consultPerson(cpfIncorrect);
    }

    @Test
    public void atualizaçãoCadastralTeste(){
        String cpf = "12345678900";

        PersonDTO personUpdate = new PersonDTO();
        personUpdate.setName("Giovanna");
        personUpdate.setCpf(cpf);

        PersonDTO existingPerson = new PersonDTO();
        existingPerson.setName("John Doe");
        existingPerson.setCpf(cpf);

        when(personService.consultPerson(cpf)).thenReturn(existingPerson);

        ResponseEntity<?> response = personController.updateRegister(cpf, personUpdate);

        Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
        verify(personService).updatePerson(cpf, personUpdate);
    }

    @Test
    public void tentativaDeAtualizacaoCadastralCpfInvalido() {
        String cpf = "12345678900";

        PersonDTO personUpdate = new PersonDTO();
        personUpdate.setName("Giovanna");
        personUpdate.setCpf(cpf);

        PersonDTO existingPerson = new PersonDTO();
        existingPerson.setName("John Doe");
        existingPerson.setCpf("60232702047");

        when(personService.consultPerson(cpf)).thenReturn(null);

        ResponseEntity<?> response = personController.updateRegister(cpf, personUpdate);

        Assertions.assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        verify(personService, never()).updatePerson(cpf, personUpdate);
    }

    @Test
    public void exclusaoDeCpfDoBancoDeDados() {
        String cpf = "12345678900";

        PersonDTO person = new PersonDTO();
        person.setName("John Doe");
        person.setCpf(cpf);

        when(personService.consultPerson(cpf)).thenReturn(person);

        ResponseEntity<?> response = personController.deletePerson(cpf);

        Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
        verify(personService).deletePerson(cpf);
    }

    @Test
    public void tentativaDeExclusaoComCpfInvalido() {
        String cpf = "0000000000";

        PersonDTO person = new PersonDTO();
        person.setName("John Doe");
        person.setCpf("12345678900");

        when(personService.consultPerson(cpf)).thenReturn(null);
        doNothing().when(personService).deletePerson(cpf);

        ResponseEntity<?> response = personController.deletePerson(cpf);

        Assertions.assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        verify(personService, never()).deletePerson(cpf);
    }


}
