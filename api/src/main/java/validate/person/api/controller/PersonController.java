package validate.person.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import validate.person.api.dto.PersonDTO;
import validate.person.api.dto.phone.PhoneDTO;
import validate.person.api.error.CpfNotFoundException;
import validate.person.api.error.InvalidCpfException;
import validate.person.api.person.Person;
import validate.person.api.person.validations.Validation;
import validate.person.api.person.validations.ValidationPhones;
import validate.person.api.service.PersonService;

import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/register")
@Controller
public class PersonController {

    @Autowired
    private PersonService service;

    @Autowired
    private List<Validation> validation;

    @Autowired
    private List<ValidationPhones> validationPhones;

    @CrossOrigin(originPatterns = "*", allowedHeaders = "*")
    @GetMapping("/registerAll")
    public ResponseEntity<?> registerAll(){
        try{
            List<PersonDTO> personDto = service.consultAllPerson();
            return ResponseEntity.ok(personDto);
        }catch (Exception e){
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(Collections.singletonMap("error", e));
        }
    }

    @CrossOrigin(originPatterns = "*", allowedHeaders = "*")
    @GetMapping("/cpf={cpf}")
    public ResponseEntity<?> register(@PathVariable String cpf) {
        try {
            PersonDTO personDto = service.consultPerson(cpf);
            if (personDto != null) {
                return ResponseEntity.ok(personDto);
            } else {
                throw new CpfNotFoundException(cpf);
            }
        } catch (CpfNotFoundException e) {
            String errorMessage = e.getMessage();
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body(Collections.singletonMap("error", errorMessage));
        }
    }

    @CrossOrigin(originPatterns = "*", allowedHeaders = "*")
    @PostMapping("/create")
    public ResponseEntity<String> createNewRegister(@RequestBody Person person) {
        try {
            validation.forEach(validation -> validation.validationCPF(person.getCpf()));
            validation.forEach(verification -> verification.verificationCPF(person.getCpf()));
            for (ValidationPhones validationPhone : validationPhones) {
                PhoneDTO phone = validationPhone.validatorPhoneNumber(person.getContacts().getPhone());
                if (!phone.isValid()) {
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Telefone não validado");
                }
            }
            service.createNewPerson(person);
            return ResponseEntity.status(HttpStatus.CREATED).body("Registro criado com sucesso!");
        } catch (InvalidCpfException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @CrossOrigin(originPatterns = "*", allowedHeaders = "*")
    @PutMapping("/cpf={cpf}")
    public ResponseEntity<?> updateRegister(@PathVariable String cpf, @RequestBody PersonDTO informationsUpdate){
        try{
            PersonDTO person = service.consultPerson(cpf);
            if (person != null) {
                PersonDTO updatePerson = service.updatePerson(cpf, informationsUpdate);
                return ResponseEntity.ok(updatePerson);
            } else {
                throw new CpfNotFoundException(cpf);
            }

        }catch (CpfNotFoundException e) {
            String errorMessage = e.getMessage();
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body(Collections.singletonMap("error", errorMessage));
        }
    }

    @CrossOrigin(originPatterns = "*", allowedHeaders = "*")
    @DeleteMapping("/cpf={cpf}")
    public ResponseEntity<?> deletePerson(@PathVariable String cpf){
        try{
            PersonDTO person = service.consultPerson(cpf);
            if (person != null) {
                service.deletePerson(cpf);
                return ResponseEntity.ok().body("Deletado com sucesso!");
            } else {
                throw new CpfNotFoundException(cpf);
            }
        }catch (CpfNotFoundException e) {
            String errorMessage = e.getMessage();
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body(Collections.singletonMap("error", errorMessage));
        }
    }

}
