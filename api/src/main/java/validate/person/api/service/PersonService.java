package validate.person.api.service;


import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import validate.person.api.dto.AddressDTO;
import validate.person.api.dto.ContactsDTO;
import validate.person.api.dto.PersonDTO;
import validate.person.api.person.Address;
import validate.person.api.person.Contacts;
import validate.person.api.person.Person;
import validate.person.api.repository.AddressRepository;
import validate.person.api.repository.PersonRepository;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PersonService {

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private ObjectMapper objectMapper;

    @Transactional
    public void createNewPerson(Person person){
        personRepository.save(person);
    }

    @Transactional
    public PersonDTO consultPerson(String cpf){
        Person person = personRepository.findByCpf(cpf);

        if (person != null) {
            PersonDTO personDTO = objectMapper.convertValue(person, PersonDTO.class);

            AddressDTO addressDTO = objectMapper.convertValue(person.getAddress(), AddressDTO.class);
            ContactsDTO contactsDTO = objectMapper.convertValue(person.getContacts(), ContactsDTO.class);
            personDTO.setAddress(addressDTO);
            personDTO.setContacts(contactsDTO);

            return personDTO;
        } else {
            return null;
        }
    }


    @Transactional
    public PersonDTO updatePerson(String cpf, PersonDTO informationsUpdate) {
        Person existingPerson = personRepository.findByCpf(cpf);

        if (existingPerson != null) {
            if (informationsUpdate.getName() != null) {
                existingPerson.setName(informationsUpdate.getName());
            }

            if (informationsUpdate.getAddress() != null) {
                AddressDTO addressUpdate = informationsUpdate.getAddress();
                Address existingAddress = existingPerson.getAddress();

                if (addressUpdate.getPublicPlace() != null) {
                    existingAddress.setPublicPlace(addressUpdate.getPublicPlace());
                }
                if (addressUpdate.getHomeNumber() != null) {
                    existingAddress.setHomeNumber(addressUpdate.getHomeNumber());
                }
                if (addressUpdate.getNeighborhood() != null) {
                    existingAddress.setNeighborhood(addressUpdate.getNeighborhood());
                }
                if (addressUpdate.getComplement() != null) {
                    existingAddress.setComplement(addressUpdate.getComplement());
                }
                if (addressUpdate.getCEP() != null) {
                    existingAddress.setCEP(addressUpdate.getCEP());
                }
            }

            if (informationsUpdate.getContacts() != null) {
                ContactsDTO contactsUpdate = informationsUpdate.getContacts();
                Contacts existingContacts = existingPerson.getContacts();

                if (contactsUpdate.getEmail() != null) {
                    existingContacts.setEmail(contactsUpdate.getEmail());
                }
                if (contactsUpdate.getPhone() != null) {
                    existingContacts.setPhone(contactsUpdate.getPhone());
                }
            }
            existingPerson = personRepository.save(existingPerson);
            return objectMapper.convertValue(existingPerson, PersonDTO.class);
        }
        return objectMapper.convertValue(existingPerson, PersonDTO.class);
    }

    @Transactional
    public void deletePerson(String cpf) {
        Person existingPerson = personRepository.findByCpf(cpf);
        personRepository.deleteById(existingPerson.getId());
    }

    public List<PersonDTO> consultAllPerson() {
        List<Person> persons = personRepository.findAll();
        return persons.stream()
                .map(person -> objectMapper.convertValue(person, PersonDTO.class))
                .collect(Collectors.toList());
    }

}
