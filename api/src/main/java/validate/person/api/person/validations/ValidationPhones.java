package validate.person.api.person.validations;

import org.springframework.stereotype.Component;
import validate.person.api.dto.phone.PhoneDTO;

@Component
public interface ValidationPhones {

    PhoneDTO validatorPhoneNumber(String phone);
}
