package validate.person.api.person;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.antlr.v4.runtime.misc.NotNull;

@Data
@Entity
@Table(name = "contacts")
@NoArgsConstructor
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler", "person"})
public class Contacts {

    @Id
    @Column(name = "ID", unique = true)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "contacts")
    @JsonIgnoreProperties("contacts")
    private Person person;

    @Column(name = "email")
    @NotNull
    String email;

    @Column(name = "phone")
    @NotNull
    String phone;
}
