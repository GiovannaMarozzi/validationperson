package validate.person.api.person.validations;

import org.springframework.stereotype.Component;

@Component
public interface Validation {
    void validationCPF(String cpf);

    void verificationCPF(String cpf);
}
