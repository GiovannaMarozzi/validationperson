package validate.person.api.person.validations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import validate.person.api.error.InvalidCpfException;
import validate.person.api.person.Person;
import validate.person.api.repository.PersonRepository;

@Component
public class ValidationCPF implements Validation {

    @Autowired
    private PersonRepository repository;

    @Override
    public void validationCPF(String cpf) {
        cpf = cpf.replaceAll("[^0-9]", "");

        if (cpf.length() != 11) {
            throw new InvalidCpfException("CPF deve ter 11 dígitos.");
        }

        if (cpf.matches("(\\d)\\1{10}")) {
            throw new InvalidCpfException("CPF inválido: todos os dígitos são iguais.");
        }

        int sum = 0;
        for (int i = 0; i < 9; i++) {
            sum += Character.getNumericValue(cpf.charAt(i)) * (10 - i);
        }
        int digit1 = (sum * 10) % 11;
        if (digit1 == 10) {
            digit1 = 0;
        }

        sum = 0;
        for (int i = 0; i < 10; i++) {
            sum += Character.getNumericValue(cpf.charAt(i)) * (11 - i);
        }
        int digit2 = (sum * 10) % 11;
        if (digit2 == 10) {
            digit2 = 0;
        }

        if (digit1 != Character.getNumericValue(cpf.charAt(9)) || digit2 != Character.getNumericValue(cpf.charAt(10))) {
            throw new InvalidCpfException("CPF inválido: dígitos verificadores não correspondem.");
        }
    }

    @Override
    public void verificationCPF(String cpf) {
        Person person = repository.findByCpf(cpf);
        if (person != null && person.getCpf() != null) {
            throw new InvalidCpfException("CPF já existente!");
        }
    }
}
