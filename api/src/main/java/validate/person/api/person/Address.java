package validate.person.api.person;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.antlr.v4.runtime.misc.NotNull;

@Data
@Entity
@Table(name = "address")
@NoArgsConstructor
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Address {

    @Id
    @Column(name = "ID", unique = true)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "address")
    @JsonIgnoreProperties("address")
    private Person person;

    @Column(name = "public_place")
    @NotNull
    String publicPlace;

    @Column(name = "number")
    @NotNull
    Integer homeNumber;

    @Column(name = "neighborhood")
    @NotNull
    String neighborhood;

    @Column(name = "complement")
    String complement;

    @Column(name = "CEP")
    @NotNull
    String CEP;
}
