package validate.person.api.person.validations;

import com.fasterxml.jackson.databind.ObjectMapper;
import jdk.swing.interop.SwingInterOpUtils;
import org.springframework.stereotype.Component;
import validate.person.api.dto.phone.PhoneDTO;
import validate.person.api.error.InvalidPhoneNumber;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


@Component
public class ValidatorPhone implements ValidationPhones {

    @Override
    public PhoneDTO validatorPhoneNumber(String phone) {
        String validationAPI = "https://phonevalidation.abstractapi.com/v1/?api_key=";
        String apiKey = "a3f3069f33fc4579aa7b979cf0bd0b13";
        String endpoint = validationAPI + apiKey + "&phone=" + phone;

        try {
            URL url = new URL(endpoint);
            System.out.println(endpoint);

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");

            int responseCode = connection.getResponseCode();

            if (responseCode == HttpURLConnection.HTTP_OK) {
                BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                String inputLine;
                StringBuilder response = new StringBuilder();

                while ((inputLine = reader.readLine()) != null) {
                    response.append(inputLine);
                }

                reader.close();
                ObjectMapper objectMapper = new ObjectMapper();
                PhoneDTO phoneDTO = objectMapper.readValue(response.toString(), PhoneDTO.class);

                return phoneDTO;
            } else if (responseCode == HttpURLConnection.HTTP_BAD_GATEWAY) {
                throw new InvalidPhoneNumber("Erro na validação do telefone. Código de resposta: 429");
            } else{
                throw new InvalidPhoneNumber("Erro na validação do telefone. Código de resposta: " + responseCode);
            }
        } catch (IOException e) {
            e.printStackTrace();
            throw new InvalidPhoneNumber("Erro na validação do telefone. Ocorreu uma exceção: " + e.getMessage());
        }
    }
}
