package validate.person.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import validate.person.api.person.Address;

public interface AddressRepository extends JpaRepository<Address, Long> {
    Address findByPersonId(Long id);
}
