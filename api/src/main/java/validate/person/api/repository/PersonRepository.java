package validate.person.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import validate.person.api.person.Person;

public interface PersonRepository extends JpaRepository<Person, Long> {

    Person findByCpf(String cpf);
}
