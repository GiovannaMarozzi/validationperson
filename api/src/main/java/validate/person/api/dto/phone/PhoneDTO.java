package validate.person.api.dto.phone;

import lombok.Data;

@Data
public class PhoneDTO {
    private String phone;
    private boolean valid;
    private FormatDTO format;
    private CountryDTO country;
    private String location;
    private String type;
    private String carrier;
}
