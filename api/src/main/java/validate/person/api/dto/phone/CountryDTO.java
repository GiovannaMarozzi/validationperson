package validate.person.api.dto.phone;

import lombok.Data;

@Data
public class CountryDTO {
    private String code;
    private String name;
    private String prefix;
}
