package validate.person.api.dto.phone;

import lombok.Data;

@Data
public class FormatDTO {
    private String international;
    private String local;
}
