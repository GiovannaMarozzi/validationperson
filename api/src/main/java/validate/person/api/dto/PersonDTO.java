package validate.person.api.dto;

import lombok.Data;
@Data
public class PersonDTO {
    String name;
    String cpf;
    String rg;
    AddressDTO address;
    String date_of_birth;
    String date_register;
    ContactsDTO contacts;
}
