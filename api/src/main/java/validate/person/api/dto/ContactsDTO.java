package validate.person.api.dto;

import lombok.Data;

@Data
public class ContactsDTO {
    private String email;
    private String phone;
}
