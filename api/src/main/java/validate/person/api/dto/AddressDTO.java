package validate.person.api.dto;

import lombok.Data;


@Data
public class AddressDTO {
    private String publicPlace;
    private Integer homeNumber;
    private String neighborhood;
    private String complement;
    private String CEP;
}
