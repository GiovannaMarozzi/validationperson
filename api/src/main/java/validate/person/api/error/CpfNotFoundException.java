package validate.person.api.error;

public class CpfNotFoundException extends RuntimeException {
    public CpfNotFoundException(String cpf) {
        super("CPF not found: " + cpf);
    }
}
