package validate.person.api.error;

public class InvalidPhoneNumber extends RuntimeException {

    public InvalidPhoneNumber(String message) {
        super(message);
    }
}
